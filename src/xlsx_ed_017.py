# дописывает в файл, генерит картинки !!спецом для ОТЛАДКИ ПРОПУСКАЕТ СТРОКУ!!!
import time
import xlrd
import xlwt3
import shutil
import os

time_start = time.time()
file_name_cfg = 'settings.cfg'


# ================================================================================
# функция резервного копирования
def backup_file(file_need_bu):
    raw_date = time.strftime("%Y, %m, %d, %H, %M, %S", time.localtime())
    date = raw_date[0:4] + raw_date[6:8] + raw_date[10:12] + '_' + raw_date[14:16] + raw_date[18:20] + raw_date[22:24]

    try:
        os.mkdir('backups')
    except FileExistsError as ex1:
        print(ex1)
        pass

    curr_bu_fold_name = 'backups/' + date

    try:
        os.mkdir(curr_bu_fold_name)
    except FileExistsError as ex2:
        print(ex2)
        pass

    shutil.copy(file_need_bu, curr_bu_fold_name)


# функция копирования заданых по ТЗ ячеек, документы и листы должны быть заранее открыты
# из столбика А [0] в price.xlsx в 1.xlsx в столбик Y [24]
# из столбика B [1] в price.xlsx в 1.xlsx в столбик W [22]
# из столбика D [3] в price.xlsx в 1.xlsx в столбик A [0]
# из столбика E [4] в price.xlsx в 1.xlsx в столбик O [14]
# из столбика G [6] в price.xlsx в 1.xlsx в столбик B [1]
# из столбика J [9] в price.xlsx в 1.xlsx в столбик F [5]

def copy_cells_tz(ws_out, row_out, ws_in, row_in, ):
    ws_in.write(row_in, 24, ws_out.cell(row_out, 0).value)
    ws_in.write(row_in, 22, ws_out.cell(row_out, 1).value)
    ws_in.write(row_in, 0, ws_out.cell(row_out, 3).value)
    ws_in.write(row_in, 14, ws_out.cell(row_out, 4).value)
    ws_in.write(row_in, 1, ws_out.cell(row_out, 6).value)
    ws_in.write(row_in, 5, ws_out.cell(row_out, 9).value)


def copy_cells_test(ws_out, row_out, ws_in, row_in, ):
    ws_in.write(row_in, 1, 'xx')
    ws_in.write(row_in, 5, 'yy')


# проверить есть ли рядом с испольняемым файлом файл настроек, если нет - создать из default_cfg.py
import default_cfg

try:
    setting = open('settings.cfg')
except FileNotFoundError:
    setting = open('settings.cfg', 'w')
    setting.write('target_file = ' + str(default_cfg.target_file) + ';\n'
                                                                    'source_file = ' + str(
        default_cfg.source_file) + ';\n'
                                   'log = ' + str(default_cfg.log) + ';\n'
                                                                     'target_list = ' + str(
        default_cfg.target_list) + ';\n'
                                   'source_list = ' + str(default_cfg.source_list) + ';\n'
                                                                                     'export_list = ' + str(
        default_cfg.export_list) + ';\n'
                                   'backup = ' + default_cfg.backup + ';\n')
    setting.close()
    setting = open('settings.cfg')

setting_dict = {}
for raw_str in setting.readlines():
    f_str = raw_str[:-1].rstrip().split(';')[0].split(' = ')
    if len(f_str) == 2:
        d = {f_str[0]: f_str[1]}
        setting_dict.update(d)

file_name_log = str(setting_dict['log'])
file_name_target = str(setting_dict['target_file'])
file_name_source = str(setting_dict['source_file'])
list_name_target = str(setting_dict['target_list'])
list_name_source = str(setting_dict['source_list'])
list_name_export = str(setting_dict['export_list'])
backup_trigger = int(setting_dict['backup'])

log = open(file_name_log, 'w')
log.write(time.ctime() + ': Start to work ... ' + '\n')
log.write(''.join(str(setting_dict)) + '\n')
log.write(file_name_target + '  ' + list_name_target)

if backup_trigger == 1:
    print('!!backuping!!')
    backup_file(file_name_target)
    backup_file(file_name_source)
    backup_file(file_name_log)

wst = xlrd.open_workbook(file_name_target).sheet_by_name(list_name_target)
target_col = wst.col_values(2)

wss = xlrd.open_workbook(file_name_source).sheet_by_name(list_name_source)
source_col = wss.col_values(0)

log.write(time.ctime() + ': ==target_col==' + '\n')
log.write(''.join(str(target_col)) + '\n')

time_fin = time.time()
delta_time = time_fin - time_start
print('работаю...', int(delta_time))
log.write(time.ctime() + ': ==in process...==' + '\n')

# сделать копию перед пере\до-записью target файла, после того как из него были считаны ключи для поиска в source
wbr = xlrd.open_workbook(file_name_target, on_demand=True, formatting_info=True)
# wse = xlwt.Worksheet(list_name_export, wbe, cell_overwrite_ok=False)

# определить N первой пустой строки в target-файл, лист= Export Products Sheet (в копии wbe читать низя, тока писать)
wsr = wbr.sheet_by_name(list_name_export)
wsr_nrow = wsr.nrows  # теперь писать в wse можно в строку номер = wsr_nrow + 1

# создать ТЕСТОВЫЙ чистый документ для отладки экспорта
# wbe = xlwt.Workbook()
# wse = wbe.add_sheet(list_name_export)
#
# wsr_nrow = 10   # TEST теперь писать в wse можно в строку номер = wsr_nrow + 1

log.write('range(target_col)' + str(len(target_col)) + ', range(source_col) = ' + str(len(source_col)) + '\n')

t = 0  # счетчик целевых ключей
e = wsr_nrow + 1  # счетчик строк в export

print(len(target_col))
files_counter = 1

# wb_result = copy(wbr)
# ws_result = wb_result.get_sheet(0)

wb_result = xlwt3.Workbook()
ws_result = wb_result.add_sheet("xz")

for target in target_col:

    if target != "":

        if target in source_col:

            row_counter = 1  # счетчик строк в source
            val_type = type(wss.cell(rowx=row_counter, colx=6).value)
            for source in source_col[1:]:
                if source == target:
                    # из столбика А [0] в price.xlsx в 1.xlsx в столбик Y [24]
                    # из столбика B [1] в price.xlsx в 1.xlsx в столбик W [22]
                    # из столбика D [3] в price.xlsx в 1.xlsx в столбик A [0]
                    # из столбика E [4] в price.xlsx в 1.xlsx в столбик O [14]
                    # из столбика G [6] в price.xlsx в 1.xlsx в столбик B [1]
                    # из столбика J [9] в price.xlsx в 1.xlsx в столбик F [5]

                    # print(row_counter, e, t)

                    log.write('s = ' + str(row_counter) + ', e = ' + str(e) + ', val = ' + str(
                        wss.cell(rowx=row_counter, colx=6).value) +
                              ', type(wss.cell(rowx= s, colx= 6).value) = ' +
                              str(type(wss.cell(rowx=row_counter, colx=6).value)) + '\n')

                    a_to_y = wss.cell(rowx=row_counter, colx=0).value
                    ws_result.write(e, 24, a_to_y)  # A => Y, float, OK

                    b_to_w = wss.cell(rowx=row_counter, colx=1).value
                    ws_result.write(e, 22, b_to_w)  # B => W, string, OK

                    d_to_a = wss.cell(rowx=row_counter, colx=3).value
                    ws_result.write(e, 0, d_to_a)  # D => A, string , OK

                    e_to_o = wss.cell(rowx=row_counter, colx=4).value
                    ws_result.write(e, 14, e_to_o)  # E => O, string, OK

                    g_to_b_orig = wss.cell(rowx=row_counter, colx=6).value
                    g_to_b = wss.cell(rowx=row_counter, colx=6).value

                    ws_result.write(e, 1, g_to_b)  # G => B, ??!!!!

                    j_to_f = wss.cell(rowx=row_counter, colx=9).value
                    ws_result.write(e, 5, j_to_f)  # J => F, NONE??, OK

                    ws_result.write(e, 4, 'r')  # => E
                    ws_result.write(e, 6, 'UAH')  # => G
                    ws_result.write(e, 8, 'шт.')  # => I
                    ws_result.write(e, 13, '+')  # => N

                    # Генератор картинки
                    product_code = wss.cell(rowx=row_counter, colx=1).value
                    penult_symb = product_code[-2:-1]
                    last_symb = product_code[-1:]

                    base_link = ('http://brain.com.ua/static/images/prod_img/' + penult_symb +
                                 '/' + last_symb + '/' + product_code + '_')

                    link_names = ", ".join([base_link + 'big.jpg',
                                            base_link + '2big.jpg',
                                            base_link + '3big.jpg',
                                            base_link + '4big.jpg',
                                            base_link + '5big.jpg'])

                    ws_result.write(e, 12, link_names)  # => M

                    e += 1

                row_counter += 1

    t += 1

wb_result.save('result.xls')

time_fin = time.time()
delta_time = time_fin - time_start
print('время работы =', int(delta_time))
log.write(time.ctime() + ': working time is ' + str(int(delta_time)) + '\n')

# TODO  гуи, параметры из консоли, компиляция,  добавить функцию копирования строки, исключить библиотеку openpyxl,
# TODO  ГЕНЕРАТОР КАРТИНОК
